package com.epam.andrei_sterkhov.third_exercise;

import com.epam.andrei_sterkhov.dto.Shop;
import com.epam.andrei_sterkhov.third_exercise.deserializers.DateDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;

public class JsonToXml {
    public static void main(String[] args) throws JAXBException, IOException {

        // 1. From Json to Java Objects with Gson
        File xmlFile = new File("src/main/java/com/epam/andrei_sterkhov/third_exercise/json.txt");
        Shop shop = null;

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDate.class, new DateDeserializer());
        Gson gson = builder.create();

        try(FileReader reader = new FileReader(xmlFile);) {
            shop = gson.fromJson(reader, Shop.class);
            System.out.println(shop);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // 1. From Json to Java Objects with Jackson
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        shop = mapper.readValue(xmlFile, Shop.class);
        System.out.println(shop);

        // 2. From Java Objects to XML with Gson
        Marshaller marshaller = null;
        try {
            JAXBContext context = JAXBContext.newInstance(Shop.class);
            marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(shop, System.out);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
