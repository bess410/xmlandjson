package com.epam.andrei_sterkhov.third_exercise;

import com.epam.andrei_sterkhov.dto.*;
import com.epam.andrei_sterkhov.third_exercise.serializers.DateSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;

public class XmlToJson {
    public static void main(String[] args) throws JAXBException, IOException {

        // 1. From XML to Java Objects
        File xmlFile = new File("src/main/java/com/epam/andrei_sterkhov/first_exercise/Test.xml");
        Shop shop = null;
        Unmarshaller unmarshaller = null;
        JAXBContext context = JAXBContext.newInstance(Shop.class);
        unmarshaller = context.createUnmarshaller();
        shop = (Shop) unmarshaller.unmarshal(xmlFile);
        System.out.println(shop);

        // 2. From Java Objects to Json with Gson
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDate.class, new DateSerializer());
        Gson gson = builder.setPrettyPrinting().create();
        String jsonString = gson.toJson(shop);
        System.out.println(jsonString);

        // 2. From Java Objects to Json with Jackson
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        jsonString = mapper.writeValueAsString(shop);
        System.out.println(jsonString);
    }
}
