@XmlSchema(
        namespace = "http://www.epam.com",
        elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED
)
package com.epam.andrei_sterkhov.dto;

import javax.xml.bind.annotation.XmlSchema;