package com.epam.andrei_sterkhov.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlType(propOrder = {"name", "categories"})
public class Shop {
    private String name;

    private List<Category> categories = new ArrayList<>();

    public List<Category> getCategories() {
        return categories;
    }

    @XmlElement(name = "category")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getName() {
        return name;
    }
    @XmlElement(name = "shop_name")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "name='" + name + '\'' +
                ", categories=" + categories +
                '}';
    }
}
