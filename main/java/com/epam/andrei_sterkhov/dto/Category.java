package com.epam.andrei_sterkhov.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlType(propOrder = {"name", "subCategories"})
public class Category {
    private String name;

    private List<SubCategory> subCategories = new ArrayList<>();

    public List<SubCategory> getSubCategories() {
        return subCategories;
    }
    @XmlElement(name = "sub_category")
    public void setSubCategories(List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }


    public String getName() {
        return name;
    }
    @XmlElement(name = "category_name")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                ", subCategories=" + subCategories +
                '}';
    }
}
