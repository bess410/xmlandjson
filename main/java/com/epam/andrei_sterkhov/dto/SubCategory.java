package com.epam.andrei_sterkhov.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlType(propOrder = {"name", "products"})
public class SubCategory {
    private String name;
    private List<Product> products = new ArrayList<>();

    public List<Product> getProducts() {
        return products;
    }
    @XmlElement(name = "product")
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getName() {
        return name;
    }
    @XmlElement(name = "sub_category_name")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SubCategory{" +
                "name='" + name + '\'' +
                ", products=" + products +
                '}';
    }
}
