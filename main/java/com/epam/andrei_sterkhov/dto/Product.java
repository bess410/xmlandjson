package com.epam.andrei_sterkhov.dto;

import com.epam.andrei_sterkhov.second_exercise.LocalDateAdapter;
import com.epam.andrei_sterkhov.third_exercise.deserializers.LocalDateDeserializer;
import com.epam.andrei_sterkhov.third_exercise.serializers.LocalDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

@XmlRootElement
@XmlType(propOrder = {"producer", "model", "dateProduce", "color", "price", "amount"})
public class Product {
    private String producer;
    private String model;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateProduce;
    private String color;
    private double price;
    private int amount;

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public LocalDate getDateProduce() {
        return dateProduce;
    }

    @XmlElement(name = "date_produce")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setDateProduce(LocalDate dateProduce) {
        this.dateProduce = dateProduce;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Product{" +
                "producer='" + producer + '\'' +
                ", model='" + model + '\'' +
                ", dateProduce=" + dateProduce +
                ", color='" + color + '\'' +
                ", price=" + price +
                ", amount=" + amount +
                "}";
    }
}
