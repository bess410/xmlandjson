package com.epam.andrei_sterkhov.second_exercise;

import com.epam.andrei_sterkhov.dto.*;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class StaxParser {
    public static void main(String[] args) throws XMLStreamException {
        Shop shop = new Shop();
        List<Category> categoryList = new ArrayList<>();
        Category currCategory = new Category();
        List<SubCategory> subCategoryList = new ArrayList<>();
        SubCategory currSubCategory = new SubCategory();
        List<Product> productList = new ArrayList<>();
        Product currProduct = new Product();
        String tagContent = "";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader =
                factory.createXMLStreamReader(
                        ClassLoader.getSystemResourceAsStream("com/epam/andrei_sterkhov/first_exercise/Test.xml"));


        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "shop":
                            categoryList = new ArrayList<>();
                            break;
                        case "category":
                            currCategory = new Category();
                            subCategoryList = new ArrayList<>();
                            break;
                        case "sub_category":
                            currSubCategory = new SubCategory();
                            productList = new ArrayList<>();
                            break;
                        case "product":
                            currProduct = new Product();
                            break;
                        default:
                    }
                    break;

                case XMLStreamConstants.CHARACTERS:
                    tagContent = reader.getText().trim();
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "producer":
                            currProduct.setProducer(tagContent);
                            break;
                        case "model":
                            currProduct.setModel(tagContent);
                            break;
                        case "date_produce":
                            currProduct.setDateProduce(LocalDate.parse(tagContent, formatter));
                            break;
                        case "color":
                            currProduct.setColor(tagContent);
                            break;
                        case "price":
                            currProduct.setPrice(Float.parseFloat(tagContent));
                            break;
                        case "amount":
                            if (!tagContent.equals("")) {
                                currProduct.setAmount(Integer.parseInt(tagContent));
                            }
                            break;
                        case "product":
                            productList.add(currProduct);
                            break;
                        case "sub_category":
                            currSubCategory.setProducts(productList);
                            subCategoryList.add(currSubCategory);
                            break;
                        case "category":
                            currCategory.setSubCategories(subCategoryList);
                            categoryList.add(currCategory);
                            break;
                        case "shop_name":
                            shop.setName(tagContent);
                            break;
                        case "category_name":
                            currCategory.setName(tagContent);
                            break;
                        case "sub_category_name":
                            currSubCategory.setName(tagContent);
                            break;
                        default:
                    }
                    break;

                case XMLStreamConstants.END_DOCUMENT:
                    shop.setCategories(categoryList);
                    break;
                default:
            }
        }
        reader.close();

        System.out.println(shop);
    }
}
